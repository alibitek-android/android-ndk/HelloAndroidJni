#include <jni.h>

JNIEXPORT jstring JNICALL
Java_ro_linuxgeek_helloandroidjni_MainActivity_getMsgFromJniC(JNIEnv *env, jobject thiz)
{
    const char* hello = "Hello world from C!";

    // add breakpoint here
    return (*env)->NewStringUTF(env, hello);
}