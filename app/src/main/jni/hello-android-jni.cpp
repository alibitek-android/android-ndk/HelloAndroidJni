#include <jni.h>
#include <string>

extern "C" jstring
Java_ro_linuxgeek_helloandroidjni_MainActivity_getMsgFromJniCpp(JNIEnv* env, jobject thiz)
{
    std::string hello = "Hello world from C++!";

    // add breakpoint here
    return env->NewStringUTF(hello.c_str());
}